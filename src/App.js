import React, { Component } from 'react';
import './App.css';
import ListPhoto from './ListPhoto';
import MenuList from './MenuList';


class App extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (

      
      <div className="App">
        <header className="App-header">
        <div id="Menu-search"></div>

        <div>
          <MenuList/>
        </div>
        
              
        </header>
      </div>
    );
  }
}

export default App;