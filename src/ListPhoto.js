import React from 'react';
import Detail from './Detail';

class ListPhoto extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: this.props.lists[0],
            createArticle: this.createArticle,
            createValid: this.createValid,
            selectedId: 0,
            x :"",
            display: false
        };
    }

    createArticle = () => {
        
        console.log("Create Article")
            this.setState({
                display: true
            });
  
    }
    createValid = () => {
        console.log("Create Valid");
        console.log(this.props.lists.push(document.getElementById("newContenu").value));
        console.log(this.props.lists[this.props.lists.length -1]);
        document.getElementById("newContenu").value="";
        //let selectedId = this.props.lists[this.props.list.length-1];
    }

    onFieldChange(event) {
        let selectedId = 0
        let selected = "";
        for (var i = 0; i < this.props.lists.length; i++)
        {
            if (this.props.lists[i].id == event.target.value)
                selected = this.props.lists[i];
                selectedId = i;
                //if (typeof x !== 'undefined') {
                    
                //}
        }
        let x = document.getElementById("title");
               x.innertext = this.props.lists[i];
               console.log(x);

        this.setState({
            selected: selected,
            selectedId: selectedId
        });
     }
     
    render () {
        let lists = this.props.lists;
        let optionItems = lists.map((item) =>
                <option key={item.id} value={item.id}>{item.title}</option>
             );
                 
        return (
         <div>
             <select onChange={this.onFieldChange.bind(this)}>
                {optionItems}
             </select>
            { this.state.display && <input id="newContenu"/> }
            
            <Detail selected={this.state.selected} createArt={this.state.createArticle} createVal={this.state.createValid} />
         </div>
        )
        
    }
}


export default ListPhoto;