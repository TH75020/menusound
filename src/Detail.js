import React from 'react';


class Detail extends React.Component {

    
    constructor(props) {
        super(props);
    }

    render () {
        let photo = "";
        let title = "";
        
        if (this.props.selected)
        {
            photo = this.props.selected.thumbnailUrl;
            title = this.props.selected.title;
        }
        
        return (
                  
            <div className="Product">
            <img src={photo} alt=""/> 
            <div className="Content"><br/>
            <div> <h1 id="title">{title}</h1></div>
            <input id="createContenu" type='button' value="Créer" onClick={this.props.createArt} />
            <input id="validContenu" type='button' value="Valider" onClick={this.props.createVal} />
            </div>
          </div>
        )
    }
}

export default Detail;