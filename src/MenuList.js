import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ListPhoto from './ListPhoto';


class MenuList extends Component {
    constructor() {
        super();
        this.state = {
            lists: [],
        };
    }

componentDidMount() {
    let ApiLists = [];
    fetch('https://jsonplaceholder.typicode.com/photos/')
        .then(response => {
            return response.json();
        }).then(data => {
        ApiLists = data.map((ListPhoto) => {
            return ListPhoto
        });
        this.setState({
            lists: ApiLists,
        });
    });
}
render() {
    return (
            <div>
                <ListPhoto lists={this.state.lists}/>
            </div>
        );
}
}
export default MenuList;
